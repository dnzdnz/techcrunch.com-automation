import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.webui.keyword.builtin.VerifyLinksAccessibleKeyword
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import cucumber.runtime.Timeout

import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.SelectorMethod

import internal.GlobalVariable

import org.openqa.selenium.By
import org.openqa.selenium.By.ByXPath
import org.openqa.selenium.Keys
import org.openqa.selenium.remote.server.handler.FindElement
import org.openqa.selenium.remote.server.handler.FindElements
import org.openqa.selenium.remote.server.handler.FindElements as Keys

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.WebElement
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

//List<WebElement> imports
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.util.KeywordUtil
import org.openqa.selenium.WebElement
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.testobject.TestObject as TestObject


WebUI.openBrowser('')

WebUI.navigateToUrl('https://techcrunch.com/')

WebUI.delay(2)

WebUI.maximizeWindow()

// To easily use XPath in findTestObject
public class TestObjectHelper {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
}

TestObject testObj1 = TestObjectHelper.getTestObjectWithXpath('.//div[@class="river river--homepage "]//article')
List<WebElement> articles = WebUI.findWebElements(testObj1, 10)
println("The Latest News count: " +articles.size())

/* Author Verification */
int missingAuthor=0;
for (int i = 1; i <= articles.size(); i++) {
	if (WebUI.verifyElementPresent(TestObjectHelper.getTestObjectWithXpath('(.//span[@class="river-byline__authors"])['+i+']//a'), 30)) {
		//println("Author present #"+i)
		if (i==articles.size() &&  missingAuthor==0) {println("Each news has an author")}
	}
	else {
		println("Author #" +i+ " is missing")
		missingAuthor++;
		if (i==articles.size()) {println("Missing author count: "+missingAuthor)}
	}
}

/* Image Verification */
int missingImg=0;
for (int n = 1;  n <= articles.size(); n++) {
	if (TestObjectHelper.getTestObjectWithXpath('.//div[@class="river river--homepage "]//article//img['+n+']')) {
		//println("Article #"+n+" has an image")
		if (n==articles.size() && missingImg==0) {println("Each news has an image")}
	}
	else {
		println("Article #"+n+" doesn't have an image")
		missingImg++;
		if (n==articles.size()) {println("Missing image count: "+missingImg)}
	}
}

/*
 * Option #2 for Image Verification
 *
if (WebUI.verifyElementPresent(TestObjectHelper.getTestObjectWithXpath('.//article[@class="post-block post-block--default post-block--unread"]'), 30)) {
	println("Image is missing for an article")
}
else {
	println("Each news has an image in The Latest News")
}
*/


// Clicking random article from The Latest News
WebElement randomElement = articles.get(new Random().nextInt(articles.size()));
randomElement.click();

WebUI.delay(2)

/* Title Verification */
String browserTitle = WebUI.getWindowTitle().substring(0, WebUI.getWindowTitle().length()-13) // substring to remove " | TechCrunch"
String articleTitle = WebUI.getText(TestObjectHelper.getTestObjectWithXpath('.//div[@class="article__title-wrapper"]/h1[@class="article__title"]'))

browserTitle == articleTitle ? println("Browser title is same with the news title") : println("Browser title is NOT same with the news title")
if (WebUI.verifyMatch(browserTitle, articleTitle, false)) {println("Titles match!")} else {println("Titles don't match!")} // Another verification


List allURLs = WebUI.getAllLinksOnCurrentPage(true, [])
println ("Total links on page: " +allURLs.size())


/* Link Verification */
int validLinks=0;
int invalidLinks=0;
TestObject testObj2 = TestObjectHelper.getTestObjectWithXpath('.//div[@class="article-content"]//p//a[starts-with(@href, "https")]')
List<WebElement> articleLinks = WebUI.findWebElements(testObj2, 10)
println ("Total links within the article content: " +articleLinks.size())
for (int j = 1;  j <= articleLinks.size(); j++) {
	if (TestObjectHelper.getTestObjectWithXpath('.//div[@class="article-content"]//p//a[starts-with(@href, "https")]['+j+']')) {
		println("Link #"+j+" is valid")
		validLinks++;
		if (j==articleLinks.size()) {println("All links inside the article content are verified\nValid link count: "+validLinks+"\nInvalid link count: "+invalidLinks)}
	}
	else {
		println("Link #"+j+" is not valid")
		invalidLinks++;
		if (j==articleLinks.size()) {println("All links inside the article content are verified\nValid link count: "+validLinks+"\nInvalid link count: "+invalidLinks)}
	}
}


/*
 * Option #2 for Link Verification - I could not get this work though
 *
WebElement element = WebUiCommonHelper.findWebElements(TestObjectHelper.getTestObjectWithXpath('.//div[@class="article-content"]//p//a[starts-with(@href,"https")]'), 30).get(0)
List URLs = Arrays.asList(element.getAttribute('href'))

println ('Total links on article content: ' +URLs.size())
WebUI.verifyLinksAccessible(URLs)

*/


WebUI.closeBrowser()